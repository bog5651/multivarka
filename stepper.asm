.include "m103def.inc"

.def posH=r16
.def temp=r17
.def posV=r18
.def tempKey=r19
.def key=r20
.def flags=r21
.def flags2=r22
.def scan=r23
.def basebuf=r24
.def flags3=r25

.def adrLowX=r26; low X
.def adrHighX=r27; High X
.def adrLowY=r28; low Y
.def adrHighY=r29; High Y
.def adrLowZ=r30; low Z
.def adrHighZ=r31; High Z

.def bufposH=r2
.def bufscan=r3

.def mode=r4;mode ����� ��������� �������� 
			;menu = 0
			;recept = 1
			;custom = 2
			;inProgress = 3

.def currentPozitionOfMenu=r5; ������� ����� ����
.def maxPointMenu=r6; ����������� ����������� �������� currentPozitionOfMenu

.def selectedPointMenu=r7; ��������� ����� ����
.def selectedCustomMenu=r8; ��������� ������ �����
					;0 - ������
					;1 - ������
					;2 - ����
.def ldiTemp=r9; temp ��� ������ ��������

.org 0x00
jmp start;
.org 0x18
jmp int_count_compare
.org 0x1c
jmp int_real_timer
.org 0x30
start:
	;����� ��������� ��������x ��� ������ ��������
	ldi basebuf, 0x60
	ldi posH, 0x0
	ldi scan, 0xEE
	ldi flags, 0x0
	ldi flags2, 0x0
	ldi flags3, 0x0

	;����� ��������� �� ��������x ��� ������ ��������
	ldi temp, 0x0;
	mov bufposH, temp;	
	mov bufscan, temp;	
	ldi temp, 0x00;
	mov mode, temp;	
	
	;��������� ��������� ��������x ���������
	ldi temp, 0x0
	out EIMSK, temp
	ldi temp, 0x14
	out TIMSK, temp
	ldi temp, 0xa8
	out OCR1AH, temp
	ldi temp, 0x9d
	out OCR1AL, temp
	ldi temp, 0xf0
	out DDRB, temp	
	ldi temp, 0x0
	out DDRD, temp
	ldi temp, 0xff
	out DDRA, temp ; �����
	ldi temp, 0x05
	out DDRE, temp ; �����
	ldi temp, LOW(RAMEND)
	out spl, temp
	ldi temp, HIGH(RAMEND)
	out sph, temp
	SEI
	ldi adrLowX, 0x45
	ldi adrHighX, 0x01
	ldi temp, 0x00
	st X, temp
	ldi adrLowX, 0x60
	ldi adrHighX, 0x01
	ldi temp, 0x03
	st X, temp
	ldi adrLowX, 0x83
	ldi adrHighX, 0x00
	ldi temp, 0x00
	st X, temp
	ldi adrLowX, 0x76
	ldi adrHighX, 0x00
	ldi temp, 0xc8
	st X, temp
	clr adrLowX
	clr adrHighX	
	// ������ ������� ��������� �������
	sBR flags3, 0x40 ;f23
	ldi temp,  0x00     ; stop timer
	out tccr1b,  temp      ; set contolling frequency to the (no input)
	ldi temp,  0xa4     ; for 6 MHz
	out tcnt1h,  temp      ; load timerH
	ldi temp,  0x72     ; for 6 MHz
	out tcnt1l,  temp      ; load timerL
	in temp, tccr1b
	ori temp,  0x04      ; start timer
	out tccr1b,  temp    ; set contolling frequency to the exact match f(OSC)/1024 (with prescaler)
	jmp fon

fon:
	SBRC flags3, 5  ;f22
		jmp proc_int_real_timer
m20:
	SBRC flags3, 7  ;f24
		jmp proc_int_count_compare
m21: 	
	jmp fon

.org 0x070f
int_real_timer:
	ldi temp,  0xa4     ; for 6 MHz
	out tcnt1h,  temp      ; load timerH
	ldi temp,  0x72     ; for 6 MHz
	out tcnt1l,  temp     ; load timerL	
	ldi temp, 0xa8
	out OCR1AH, temp
	ldi temp, 0x9b
	out OCR1AL, temp
	in temp, tccr1b
	SBR flags3, 0x20 ;f22
	reti

int_count_compare:
	SBR flags3, 0x80 ;f24
	in ldiTemp, TCNT1L
	ldi temp, 0x29
	add temp, ldiTemp
	BRCS increment_OCR1AH
	jmp after_increment_OCR1AH
	increment_OCR1AH:
		out OCR1AL, temp
		in ldiTemp, TCNT1H		
		ldi temp, 0x05
		add temp, ldiTemp
		out OCR1AH, temp
		reti
	after_increment_OCR1AH:
		out OCR1AL, temp
		in ldiTemp, TCNT1H		
		ldi temp, 0x04
		add temp, ldiTemp
		out OCR1AH, temp
		reti

proc_int_count_compare:
	CBR flags3, 0x80 ;f24
	ldi adrLowX, 0x60
	ldi adrHighX, 0x01
	ld temp, X
	SBRC temp, 0  ;160.0
	jmp make_step
m0: 
	jmp m21
	
make_step:	
	ldi adrLowX, 0x83
	ldi adrHighX, 0x00
	ld temp, X
	inc temp
	cpi temp, 0x08
	breq overflow
	jmp after_overflow
overflow:
	ldi temp, 0x00
after_overflow:
	ldi adrLowX, 0x83
	ldi adrHighX, 0x00
	st X, temp

	LDI ZL, LOW(shift*2)
	LDI ZH, HIGH(shift*2)
	add ZL, temp;
	add ZL, temp;
	brcs increment_ZH_2;
	jmp after_increment_zh_2;
	increment_ZH_2: inc ZH
	after_increment_zh_2: 
	LPM
	mov temp, r0
	lsl temp
	lsl temp
	lsl temp
	lsl temp
	out PORTE, temp
	jmp m0;	

proc_int_real_timer:
	CBR flags3, 0x20
	ldi adrLowX, 0x60
	ldi adrHighX, 0x01
	ld temp, X
	SBRC temp, 0  ;160.0
	jmp dec_stepper_timer
m1_1:
	jmp m20
dec_stepper_timer:
	ldi adrLowX, 0x80
	ldi adrHighX, 0x00
	ld temp, X
	dec temp
	cpi temp, 0x00
	breq stop_stepper
	st X, temp
	jmp m1_1
stop_stepper:
	ldi adrLowX, 0x60
	ldi adrHighX, 0x01
	ld temp, X
	CBR temp, 0x01
	st X, temp
	jmp m1_1

shift:
	.db 0x08
	.db 0x0c
	.db 0x04
	.db 0x06
	.db 0x02
	.db 0x03
	.db 0x01
	.db 0x09

































