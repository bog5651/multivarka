.include "m103def.inc"

.def posH=r16
.def temp=r17
.def posV=r18
.def tempKey=r19
.def key=r20
.def flags=r21
.def flags2=r22
.def scan=r23
.def basebuf=r24
.def flags3=r25

.def adrLowX=r26; low X
.def adrHighX=r27; High X
.def adrLowY=r28; low Y
.def adrHighY=r29; High Y
.def adrLowZ=r30; low Z
.def adrHighZ=r31; High Z

.def bufposH=r2
.def bufscan=r3

.def mode=r4;mode ����� ��������� �������� 
			;menu = 0
			;recept = 1
			;custom = 2
			;inProgress = 3

.def currentPozitionOfMenu=r5; ������� ����� ����
.def maxPointMenu=r6; ����������� ����������� �������� currentPozitionOfMenu

.def selectedPointMenu=r7; ��������� ����� ����
.def customMenu=r8; ��������� ������ �����
					;0 - ������
					;1 - ������
					;2 - ����

.org 0x00
jmp start;
.org 0x30
start:
	;����� ��������� ��������� ��� ������ ��������
	ldi basebuf, 0x60
	ldi posH, 0x0
	ldi scan, 0xEE
	ldi flags, 0x0
	ldi flags2, 0x0
	ldi flags3, 0x0

	;����� ��������� �� ��������� ��� ������ ��������
	ldi temp, 0x0;
	mov bufposH, temp;	
	mov bufscan, temp;	
	ldi temp, 0x00;
	mov mode, temp;	
	
	;��������� ��������� ��������� ���������
	ldi temp, 0x0
	out EIMSK, temp
	ldi temp, 0x41
	out TIMSK, temp
	ldi temp, 0xf0
	out DDRB, temp	
	ldi temp, 0x0
	out DDRD, temp
	ldi temp, LOW(RAMEND)
	out spl, temp
	ldi temp, HIGH(RAMEND)
	out sph, temp
	SEI
	jmp key_back

.org 0x0106
key_back:
	CBR flags, 0x40  ;set flags3.3 = 1
	mov temp, mode;
	LSL	temp; x2
	ldi ZH, 0x00;
	mov ZL, temp

	subi ZL, low (-Table_key_back)
	sbci ZH, high (-Table_key_back)
	ijmp
		mode_menu:
			jmp end_switch_key_back;
		mode_recept:
			ldi temp, 0x01;
			mov mode, temp;
			jmp end_switch_key_back;
		mode_custom:
			ldi temp, 0x01;
			mov mode, temp;
			jmp end_switch_key_back;
		mode_in_progress:
			SBR flags3, 0x08  ;set flags3.3 = 1
			jmp end_switch_key_back;
	end_switch_key_back: nop
	jmp start;jmp fon;
endkey_back: nop;

Table_key_back:
jmp mode_menu;
jmp mode_recept;
jmp mode_custom;
jmp mode_in_progress;
endTable: nop;
