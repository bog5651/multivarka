.include "m103def.inc"

.def posH=r16
.def temp=r17
.def posV=r18
.def tempKey=r19
.def key=r20
.def flags=r21
.def flags2=r22
.def scan=r23
.def basebuf=r24
.def flags3=r25

.def adrLowX=r26; low X
.def adrHighX=r27; High X
.def adrLowY=r28; low Y
.def adrHighY=r29; High Y
.def adrLowZ=r30; low Z
.def adrHighZ=r31; High Z

.def bufposH=r2
.def bufscan=r3

.def mode=r4;mode ����� ��������� �������� 
			;menu = 0
			;recept = 1
			;custom = 2
			;inProgress = 3

.def currentPozitionOfMenu=r5; ������� ����� ����
.def maxPointMenu=r6; ����������� ����������� �������� currentPozitionOfMenu

.def selectedPointMenu=r7; ��������� ����� ����
.def customMenu=r8; ��������� ������ �����
					;0 - ������
					;1 - ������
					;2 - ����

.org 0x00
jmp start;
.org 0x30
start:
	;����� ��������� ��������� ��� ������ ��������
	ldi basebuf, 0x60
	ldi posH, 0x0
	ldi scan, 0xEE
	ldi flags, 0x0
	ldi flags2, 0x0
	ldi flags3, 0x0

	;����� ��������� �� ��������� ��� ������ ��������
	ldi temp, 0x0;
	mov bufposH, temp;	
	mov bufscan, temp;	
	ldi temp, 0x00;
	mov mode, temp;	
	ldi temp, 0x00;
	mov customMenu, temp;	
	ldi temp, 0x30;
	mov maxPointMenu, temp;
	
	;��������� ��������� ��������� ���������
	ldi temp, 0x0
	out EIMSK, temp
	ldi temp, 0x41
	out TIMSK, temp
	ldi temp, 0xf0
	out DDRB, temp	
	ldi temp, 0x0
	out DDRD, temp
	ldi temp, LOW(RAMEND)
	out spl, temp
	ldi temp, HIGH(RAMEND)
	out sph, temp
	SEI
	jmp key_up

.org 0x01c0
key_up:
	CBR flags, 0x8 ;set flags.3 = 1
	mov temp, mode;
	LSL	temp; x2
	ldi ZH, 0x00;
	mov ZL, temp
	subi ZL, low (-Table_key_up)
	sbci ZH, high (-Table_key_up)
	ijmp
		mode_menu:
			ldi temp, 0x00;
			cp currentPozitionOfMenu, temp;
			BREQ equals
			jmp not_equals
			not_equals:
				dec currentPozitionOfMenu;
				jmp end_if_mode_menu_key_up;
			equals:
				mov temp, maxPointMenu
				mov currentPozitionOfMenu, temp;
				jmp end_if_mode_menu_key_up;
			end_if_mode_menu_key_up:
			jmp end_switch_key_up;
		mode_recept:
			jmp end_switch_key_up;
		mode_custom:
			mov temp, customMenu;
			LSL	temp; x2
			ldi ZH, 0x00;
			mov ZL, temp
			subi ZL, low (-Table_key_up_custom_mode)
			sbci ZH, high (-Table_key_up_custom_mode)
			ijmp
				heating:
					clr adrHighX;
					ldi adrLowX, 0x70;load from data
					ld temp, X;
					inc temp
					st X, temp ;save to data
					jmp end_switch_mode_custom
				mixing:
					clr adrHighX;
					ldi adrLowX, 0x80;load from data
					ld temp, X;
					inc temp
					st X, temp;save to data
					jmp end_switch_mode_custom
				weigher: ;����
					jmp end_switch_mode_custom
				end_switch_mode_custom:
					jmp end_switch_key_up;
			jmp end_switch_key_up;
		mode_in_progress:
			jmp end_switch_key_up;
	end_switch_key_up: nop
	jmp start;jmp fon;
endkey_up: nop;

Table_key_up:
jmp mode_menu;
jmp mode_recept;
jmp mode_custom;
jmp mode_in_progress;
endTable: nop;

Table_key_up_custom_mode:
jmp heating
jmp mixing
jmp weigher
end_Table_key_up_custom_mode: nop;
