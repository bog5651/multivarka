.include "m103def.inc"

.def posH=r16
.def temp=r17
.def posV=r18
.def tempKey=r19
.def key=r20
.def flags=r21
.def flags2=r22
.def scan=r23
.def basebuf=r24
.def flags3=r25

.def adrLowX=r26; low X
.def adrHighX=r27; High X
.def adrLowY=r28; low Y
.def adrHighY=r29; High Y
.def adrLowZ=r30; low Z
.def adrHighZ=r31; High Z

.def bufposH=r2
.def bufscan=r3

.def mode=r4;mode ����� ��������� �������� 
			;menu = 0
			;recept = 1
			;custom = 2
			;inProgress = 3

.def currentPozitionOfMenu=r5; ������� ����� ����
.def maxPointMenu=r6; ����������� ����������� �������� currentPozitionOfMenu

.def selectedPointMenu=r7; ��������� ����� ����
.def customMenu=r8; ��������� ������ �����
					;0 - ������
					;1 - ������
					;2 - ����

.org 0x00
jmp start
.org 0x2
jmp intKey
.org 0x14
jmp intTD
.org 0x30
start:
	;����� ��������� ��������� ��� ������ ��������
	ldi basebuf, 0x60
	ldi posH, 0x0
	ldi scan, 0xEE
	ldi flags, 0x0
	ldi flags2, 0x0
	ldi flags3, 0x0

	;����� ��������� �� ��������� ��� ������ ��������
	ldi temp, 0x0;
	mov bufposH, temp;	
	mov bufscan, temp;	
	ldi temp, 0x01;
	mov mode, temp;	
	
	;��������� ��������� ��������� ���������
	ldi temp, 0x1
	out EIMSK, temp
	ldi temp, 0x41
	out TIMSK, temp
	ldi temp, 0xf0
	out DDRB, temp	
	ldi temp, 0x0
	out DDRD, temp
	ldi temp, LOW(RAMEND)
	out spl, temp
	ldi temp, HIGH(RAMEND)
	out sph, temp
	SEI
fon:
	SBRC flags, 0 ;f0
	jmp proc_drebezg
m1: SBRC flags, 1
	jmp proc_key
m2: SBRC flags, 2
	jmp proc_define
m3: SBRC flags, 3
	jmp key_up
m4: SBRC flags, 4
	jmp key_down
m5: SBRC flags, 5
	jmp key_select
m6: SBRC flags, 6
	jmp key_back
m7: SBRC flags, 7
	jmp key_1
m8: SBRC flags2, 0 ;f8
	jmp key_2
m9: SBRC flags2, 1
	jmp key_3
m10: SBRC flags2, 2
	jmp key_4
m11: SBRC flags2, 3
	jmp key_5
m12: SBRC flags2, 4
	jmp key_6
m13: SBRC flags2, 5
	jmp key_7
m14: SBRC flags2, 6
	jmp key_8
m15: SBRC flags2, 7
	jmp key_9
m16: SBRC flags3, 0 ;f16
	jmp key_0
m17: SBRC flags3, 1 ;f17
	jmp start_recept
m18: SBRC flags3, 2 ;f18
	jmp proc_tick
m19: SBRC flags3, 3 ;f19
	jmp stop_recept
;m20: SBRC flags3, 4 ;f21
;m21: SBRC flags3, 5  ;f22
;m22: SBRC flags3, 6 ;f23
;m23: SBRC flags3, 7 ;24
	call update_display

proc_drebezg:
	CBR flags, 0x1 ;clear flags.1 = 0
	;start timer TD
	ldi temp, 0xf0; �������� ������� 2
	out tcnt2, temp
	ldi temp, 0xf7; ��������� �������� �������� �������
	out xdiv, temp
	ldi temp, 0x02; ������ TD
	out tccr2, temp
	ldi temp, 0x40; ���������� ���������� ���������� �� ������������
	out timsk, temp
	clt; ����� �����	
	jmp m1
proc_key: 
	CBR flags, 0x2 ;clear flags.1 = 0
	mov scan, bufscan
	out PORTA, scan	;portA:=scan
	in tempKey, PINB	;tempKey:=portB
	ldi posV, 0x0
	;���� �
	cycleProcK:
	lsr tempKey
	brcc procKeyContinue
	inc posV
	jmp cycleProcK
	;���� �
procKeyContinue:
	mov posH, bufposH
	lsl posH
	lsl posH
	or posH, posV
	mov key, posH
	mov adrLowX, basebuf
	st X,key
	SBR flags, 0x4 ;set flags.2 = 1	
	clt; ����� �����	
	jmp fon
intKey:
	SBR flags, 0x1 ;set flags.0 = 1
	mov bufscan, scan
	mov bufposH, posH
	;ldi temp, 0x07
	;mov bufscan, temp
	;ldi temp, 0x00
	;mov bufposH, temp
	ldi temp, 0x0
	out EIMSK, temp
	reti
intTD:
	;ldi temp, 0x1; allow intKey
	;out EIMSK, temp
	SBR flags, 0x2 ;set flags.1 = 1
	ldi temp, 0x00;
	out tccr0, temp
	SBR flags, 0x2 
	reti
proc_define:
	CBR flags, 0x4 ;clear flags.3 = 1
	call doOnePik;

	mov adrLowX, basebuf;load from data
	ld temp, X;
	;in temp loaded readed key
		LSL	temp; x2
		ldi ZH, 0x00;
		mov ZL, temp

		subi ZL, low (-Table)
		sbci ZH, high (-Table)

		ijmp
		;cases
			Key0:
				SBR flags3, 0x00 ;set flags3.0 = 1
				jmp endSwitch;
			Key1:
				SBR flags, 0x80 ;set flags.7 = 1
				jmp endSwitch;
			Key2:
				SBR flags2, 0x00 ;set flags2.0 = 1
				jmp endSwitch;
			Key3:
				SBR flags2, 0x02 ;set flags2.1 = 1
				jmp endSwitch;
			Key4:
				SBR flags2, 0x04 ;set flags2.2 = 1
				jmp endSwitch;
			Key5:
				SBR flags2, 0x08 ;set flags2.3 = 1
				jmp endSwitch;
			Key6:
				SBR flags2, 0x10 ;set flags2.4 = 1
				jmp endSwitch;
			Key7:
				SBR flags2, 0x20 ;set flags2.5 = 1
				jmp endSwitch;
			Key8:
				SBR flags2, 0x40 ;set flags2.6 = 1
				jmp endSwitch;
			Key9:
				SBR flags2, 0x80 ;set flags2.7 = 1
				jmp endSwitch;
			KeySelect:
				SBR flags, 0x20 ;set flags.5 = 1
				jmp endSwitch;
			KeyBack:
				SBR flags, 0x40 ;set flags.6 = 1
				jmp endSwitch;
			KeyUp:
				SBR flags, 0x08 ;set flags.3 = 1
				jmp endSwitch;
			KeyDown:
				SBR flags, 0x10 ;set flags.4 = 1
				jmp endSwitch;

		endSwitch:	nop	;end switch
	jmp fon

Table:
jmp	Key0;
jmp Key1;
jmp Key2;
jmp Key3;
jmp Key4;
jmp Key5;
jmp Key6;
jmp Key7;
jmp Key8;
jmp Key9;
jmp KeySelect;
jmp KeyBack;
jmp KeyUp;
jmp KeyDown;
	check:
key_up:
	CBR flags, 0x8 ;set flags.3 = 1
	jmp m4
key_down:
	CBR flags, 0x10 ;set flags.4 = 1
	jmp m5
key_select:
	CBR flags, 0x20 ;set flags.5 = 1
	jmp m6
key_back:
	CBR flags, 0x40 ;set flags.6 = 1
	jmp m7
key_0:
	CBR flags3, 0x00 ;set flags3.0 = 1
	jmp m17
key_1:
	CBR flags, 0x80 ;set flags.7 = 1
	jmp m8
key_2:
	CBR flags2, 0x00 ;set flags2.0 = 1
	jmp m9
key_3:
	CBR flags2, 0x2 ;set flags2.1 = 1
	jmp m10
key_4:
	CBR flags2, 0x4 ;set flags2.2 = 1
	jmp m11
key_5:
	CBR flags2, 0x8 ;set flags2.3 = 1
	jmp m12
key_6:
	CBR flags2, 0x10 ;set flags2.4 = 1
	jmp m13
key_7:
	CBR flags2, 0x20 ;set flags2.5 = 1
	jmp m14
key_8:
	CBR flags2, 0x40 ;set flags2.6 = 1
	jmp m15
key_9:
	CBR flags2, 0x80 ;set flags2.7 = 1
	jmp m16
start_recept:
	jmp m18
stop_recept:
	jmp fon
proc_tick:
	jmp m19
update_display:
	ret
doOnePik:
	ret
turnOffPik:
	ret;






;do u now the way





