.include "m128def.inc"

.def posH=r16
.def temp=r17
.def posV=r18
.def tempKey=r19
.def key=r20
.def flags=r21
.def flags2=r22
.def scan=r23
.def basebuf=r24
.def flags3=r25

.def adrLowX=r26; low X
.def adrHighX=r27; High X
.def adrLowY=r28; low Y
.def adrHighY=r29; High Y
.def adrLowZ=r30; low Z
.def adrHighZ=r31; High Z

.def bufposH=r2
.def bufscan=r3

.def mode=r4;mode ����� ��������� �������� 
			;menu = 0
			;recept = 1
			;custom = 2
			;inProgress = 3

.def currentPozitionOfMenu=r5; ������� ����� ����
.def maxPointMenu=r6; ����������� ����������� �������� currentPozitionOfMenu

.def selectedPointMenu=r7; ��������� ����� ����
.def selectedCustomMenu=r8; ��������� ������ �����
					;0 - ������
					;1 - ������
					;2 - ����
.def ldiTemp=r9; temp ��� ������ ��������

.org 0x00
jmp start;
.org 0x30
start:
	;����� ��������� ��������x ��� ������ ��������
	ldi basebuf, 0x60
	ldi posH, 0x0
	ldi scan, 0xEE
	ldi flags, 0x0
	ldi flags2, 0x0
	ldi flags3, 0x0

	;����� ��������� �� ��������x ��� ������ ��������
	ldi temp, 0x0;
	mov bufposH, temp;	
	mov bufscan, temp;	
	ldi temp, 0x00;
	mov mode, temp;	
	
	;��������� ��������� ��������x ���������
	ldi temp, 0x0
	out EIMSK, temp
	ldi temp, 0x41
	out TIMSK, temp
	ldi temp, 0xf0
	out DDRB, temp	
	ldi temp, 0x0
	out DDRD, temp
	ldi temp, 0xff
	out DDRA, temp ; �����
	ldi temp, 0x05
	out DDRE, temp ; �����
	ldi temp, LOW(RAMEND)
	out spl, temp
	ldi temp, HIGH(RAMEND)
	out sph, temp
	SEI
	ldi adrLowX, 0x45
	ldi adrHighX, 0x01
	ldi temp, 0x00
	st X, temp
	clr adrLowX
	clr adrHighX
	jmp update_display
	;jmp update_display

.org 0x0239
update_display:
	;init_display:	
	ldi temp, 0b00110000
	;send_cmd
	in adrLowY, PORTE	;tempKey:=portB
	andi adrLowY, ~(1<<0)
	out PORTE, adrLowY	;portA:=scan
	ldi adrLowY, 0;

	out PORTD, temp;
	;pulse
 	in adrLowY, PORTE	;tempKey:=portB
	ori adrLowY, (1<<2)
	out PORTE, adrLowY	;portA:=scan
	ldi adrLowY, 0;

 	ldi temp, 0x06
	delay_us_3:
		nop
		dec temp;
		cpi temp, 0;
		brne delay_us_3;

	in adrLowY, PORTE	;tempKey:=portB
	andi adrLowY, ~(1<<2)
	out PORTE, adrLowY	;portA:=scan
	ldi adrLowY, 0;

	ldi temp, 0x06
	delay_us_4:
		nop
		dec temp;
		cpi temp, 0;
		brne delay_us_4
	ldi temp, 0x10
	delay_us_0:
		nop
		dec temp;
		cpi temp, 0;
		brne delay_us_0;
	
	ldi temp, 0b00000001
	;send_cmd
	in adrLowY, PORTE	;tempKey:=portB
	andi adrLowY, ~(1<<0)
	out PORTE, adrLowY	;portA:=scan
	ldi adrLowY, 0;

	out PORTD, temp;
	;pulse
 	in adrLowY, PORTE	;tempKey:=portB
	ori adrLowY, (1<<2)
	out PORTE, adrLowY	;portA:=scan
	ldi adrLowY, 0;

 	ldi temp, 0x06
	delay_us_9:
		nop
		dec temp;
		cpi temp, 0;
		brne delay_us_9;

	in adrLowY, PORTE	;tempKey:=portB
	andi adrLowY, ~(1<<2)
	out PORTE, adrLowY	;portA:=scan
	ldi adrLowY, 0;

	ldi temp, 0x06
	delay_us_10:
		nop
		dec temp;
		cpi temp, 0;
		brne delay_us_10
	ldi temp, 0x06
	delay_us_1:
		nop
		dec temp;
		cpi temp, 0;
		brne delay_us_1;
	
	ldi temp, 0b00001100
	;send_cmd
	in adrLowY, PORTE	;tempKey:=portB
	andi adrLowY, ~(1<<0)
	out PORTE, adrLowY	;portA:=scan
	ldi adrLowY, 0;

	out PORTD, temp;
	;pulse
 	in adrLowY, PORTE	;tempKey:=portB
	ori adrLowY, (1<<2)
	out PORTE, adrLowY	;portA:=scan
	ldi adrLowY, 0;

 	ldi temp, 0x06
	delay_us_11:
		nop
		dec temp;
		cpi temp, 0;
		brne delay_us_11;

	in adrLowY, PORTE	;tempKey:=portB
	andi adrLowY, ~(1<<2)
	out PORTE, adrLowY	;portA:=scan
	ldi adrLowY, 0;

	ldi temp, 0x06
	delay_us_12:
		nop
		dec temp;
		cpi temp, 0;
		brne delay_us_12
	ldi temp, 0x08
	delay_us_2:
		nop
		dec temp;
		cpi temp, 0;
		brne delay_us_2;
	mov temp, mode;
	LSL	temp; x2
	ldi ZH, 0x00;
	mov ZL, temp

	subi ZL, low (-Table_update_display_main_switch)
	sbci ZH, high (-Table_update_display_main_switch)
	ijmp
		mode_menu:
			LDI ZL, LOW(menu_text*2)
			LDI ZH, HIGH(menu_text*2)
			ldi temp, 0x10;
			mul temp, currentPozitionOfMenu
			add ZL, r0;
			brvs increment_ZH;
			jmp after_increment_zh;
			increment_ZH: inc ZH
			after_increment_zh:
			;set_line_0:
			ldi temp,0x80			    
			;send_cmd
			in adrLowY, PORTE	;tempKey:=portB
			andi adrLowY, ~(1<<0)
			out PORTE, adrLowY	;portA:=scan
			ldi adrLowY, 0;

			out PORTD, temp;
			;pulse
		 	in adrLowY, PORTE	;tempKey:=portB
			ori adrLowY, (1<<2)
			out PORTE, adrLowY	;portA:=scan
			ldi adrLowY, 0;

		 	ldi temp, 0x06
			delay_us_13:
				nop
				dec temp;
				cpi temp, 0;
				brne delay_us_13;

			in adrLowY, PORTE	;tempKey:=portB
			andi adrLowY, ~(1<<2)
			out PORTE, adrLowY	;portA:=scan
			ldi adrLowY, 0;

			ldi temp, 0x06
			delay_us_14:
				nop
				dec temp;
				cpi temp, 0;
				brne delay_us_14
			;send_string: ;�������������� �������� ��� ������
				ldi temp, 0x10;���������� ��������� � ������
				mov ldiTemp, temp
			m1:	
				nop
				in adrLowX, PORTE	;tempKey:=portB
				ori adrLowX, (1<<2)
				out PORTE, adrLowX	;portA:=scan
				ldi adrLowX, 0
	 
				LPM 
				inc ZL
				BRVS increm_ZH
				jmp afterinc_
				increm_ZH:	inc ZH	
				afterinc_:
				out PORTD, r0;
			 	;pulse
			 	in adrLowY, PORTE	;tempKey:=portB
				ori adrLowY, (1<<2)
				out PORTE, adrLowY	;portA:=scan
				ldi adrLowY, 0;

			 	ldi temp, 0x06
				delay_us_7:
					nop
					dec temp;
					cpi temp, 0;
					brne delay_us_7;

				in adrLowY, PORTE	;tempKey:=portB
				andi adrLowY, ~(1<<2)
				out PORTE, adrLowY	;portA:=scan
				ldi adrLowY, 0;

				ldi temp, 0x06
				delay_us_8:
					nop
					dec temp;
					cpi temp, 0;
					brne delay_us_8;

				dec ldiTemp
				ldi temp, 0
				cp ldiTemp, temp
				brne m1
			;set_line_1:
			ldi temp,0xc2			    
			;send_cmd
			in adrLowY, PORTE	;tempKey:=portB
			andi adrLowY, ~(1<<0)
			out PORTE, adrLowY	;portA:=scan
			ldi adrLowY, 0;

			out PORTD, temp;
			;pulse
		 	in adrLowY, PORTE	;tempKey:=portB
			ori adrLowY, (1<<2)
			out PORTE, adrLowY	;portA:=scan
			ldi adrLowY, 0;

		 	ldi temp, 0x06
			delay_us_15:
				nop
				dec temp;
				cpi temp, 0;
				brne delay_us_15;

			in adrLowY, PORTE	;tempKey:=portB
			andi adrLowY, ~(1<<2)
			out PORTE, adrLowY	;portA:=scan
			ldi adrLowY, 0;

			ldi temp, 0x06
			delay_us_16:
				nop
				dec temp;
				cpi temp, 0;
				brne delay_us_16
			;send_string: ;�������������� �������� ��� ������
				ldi temp, 0x10;���������� ��������� � ������
				mov ldiTemp, temp
			m2:	
				nop
				in adrLowX, PORTE	;tempKey:=portB
				ori adrLowX, (1<<2)
				out PORTE, adrLowX	;portA:=scan
				ldi adrLowX, 0
	 
				LPM 
				inc ZL
				BRVS increm_ZH_1
				jmp afterinc_1
				increm_ZH_1:	inc ZH	
				afterinc_1:
				out PORTD, r0;
			 	;pulse
			 	in adrLowY, PORTE	;tempKey:=portB
				ori adrLowY, (1<<2)
				out PORTE, adrLowY	;portA:=scan
				ldi adrLowY, 0;

			 	ldi temp, 0x06
				delay_us_25:
					nop
					dec temp;
					cpi temp, 0;
					brne delay_us_25;

				in adrLowY, PORTE	;tempKey:=portB
				andi adrLowY, ~(1<<2)
				out PORTE, adrLowY	;portA:=scan
				ldi adrLowY, 0;

				ldi temp, 0x06
				delay_us_26:
					nop
					dec temp;
					cpi temp, 0;
					brne delay_us_26;

				dec ldiTemp
				ldi temp, 0
				cp ldiTemp, temp
				brne m2
			;set_line_0:
			ldi temp,0x80			    
			;send_cmd
			in adrLowY, PORTE	;tempKey:=portB
			andi adrLowY, ~(1<<0)
			out PORTE, adrLowY	;portA:=scan
			ldi adrLowY, 0;

			out PORTD, temp;
			;pulse
		 	in adrLowY, PORTE	;tempKey:=portB
			ori adrLowY, (1<<2)
			out PORTE, adrLowY	;portA:=scan
			ldi adrLowY, 0;

		 	ldi temp, 0x06
			delay_us_17:
				nop
				dec temp;
				cpi temp, 0;
				brne delay_us_17;

			in adrLowY, PORTE	;tempKey:=portB
			andi adrLowY, ~(1<<2)
			out PORTE, adrLowY	;portA:=scan
			ldi adrLowY, 0;

			ldi temp, 0x06
			delay_us_18:
				nop
				dec temp;
				cpi temp, 0;
				brne delay_us_18
			jmp end_main_switch;
		mode_recept:
			LDI ZL, LOW(menu_text*2)
			LDI ZH, HIGH(menu_text*2)
			ldi temp, 0x10;
			mul temp, currentPozitionOfMenu
			bclr 3
			add ZL, r0;
			brcs increment_ZH_1;
			jmp after_increment_zh_1;
			increment_ZH_1: inc ZH
			after_increment_zh_1:
			;set_line_0:
			ldi temp,0x80			    
			;send_cmd
			in adrLowY, PORTE	;tempKey:=portB
			andi adrLowY, ~(1<<0)
			out PORTE, adrLowY	;portA:=scan
			ldi adrLowY, 0;

			out PORTD, temp;
			;pulse
		 	in adrLowY, PORTE	;tempKey:=portB
			ori adrLowY, (1<<2)
			out PORTE, adrLowY	;portA:=scan
			ldi adrLowY, 0;

		 	ldi temp, 0x06
			delay_us_19:
				nop
				dec temp;
				cpi temp, 0;
				brne delay_us_19;

			in adrLowY, PORTE	;tempKey:=portB
			andi adrLowY, ~(1<<2)
			out PORTE, adrLowY	;portA:=scan
			ldi adrLowY, 0;

			ldi temp, 0x06
			delay_us_20:
				nop
				dec temp;
				cpi temp, 0;
				brne delay_us_20
			;send_string: ;�������������� �������� ��� ������
				ldi temp, 0x10;���������� ��������� � ������
				mov ldiTemp, temp
			m3:	
				nop
				in adrLowX, PORTE	;tempKey:=portB
				ori adrLowX, (1<<2)
				out PORTE, adrLowX	;portA:=scan
				ldi adrLowX, 0
	 
				LPM 
				inc ZL
				BRCS increm_ZH_2
				jmp afterinc_2
				increm_ZH_2:	inc ZH	
				afterinc_2:
				out PORTD, r0;
			 	;pulse
			 	in adrLowY, PORTE	;tempKey:=portB
				ori adrLowY, (1<<2)
				out PORTE, adrLowY	;portA:=scan
				ldi adrLowY, 0;

			 	ldi temp, 0x06
				delay_us_27:
					nop
					dec temp;
					cpi temp, 0;
					brne delay_us_27;

				in adrLowY, PORTE	;tempKey:=portB
				andi adrLowY, ~(1<<2)
				out PORTE, adrLowY	;portA:=scan
				ldi adrLowY, 0;

				ldi temp, 0x06
				delay_us_28:
					nop
					dec temp;
					cpi temp, 0;
					brne delay_us_28;

				dec ldiTemp
				ldi temp, 0
				cp ldiTemp, temp
				brne m3
			;set_line_1:
			ldi temp,0xc2			    
			;send_cmd
			in adrLowY, PORTE	;tempKey:=portB
			andi adrLowY, ~(1<<0)
			out PORTE, adrLowY	;portA:=scan
			ldi adrLowY, 0;

			out PORTD, temp;
			;pulse
		 	in adrLowY, PORTE	;tempKey:=portB
			ori adrLowY, (1<<2)
			out PORTE, adrLowY	;portA:=scan
			ldi adrLowY, 0;

		 	ldi temp, 0x06
			delay_us_23:
				nop
				dec temp;
				cpi temp, 0;
				brne delay_us_23;

			in adrLowY, PORTE	;tempKey:=portB
			andi adrLowY, ~(1<<2)
			out PORTE, adrLowY	;portA:=scan
			ldi adrLowY, 0;

			ldi temp, 0x06
			delay_us_24:
				nop
				dec temp;
				cpi temp, 0;
				brne delay_us_24

			ldi adrLowX, 0x45;load from data
			ldi adrHighX, 0x1; itogo 0x100
			ld temp, X;
			clr adrLowX;
			clr adrHighX;

			CPI temp, 0x00
			breq if_not_calc_time;
			jmp if_calc;
				if_not_calc_time:
				SBR flags3, 0x08
				jmp end_main_switch; ;flags3.4 set 1
			if_calc:
					ldi basebuf, 0x92
				cycle_mode_recept:
				    cpi temp, 0x0a
				    brsh q1_mode_recept
				    jmp quit1_mode_recept
				q1_mode_recept:
				    subi temp, 0x0a
				    inc r1     
				    jmp cycle_mode_recept
				quit1_mode_recept:
				    mov adrLowX, basebuf
					ori temp, 0x30
				    st X,temp
				    mov temp, r1
				    clr r1
				cycle1_mode_recept:
				    cpi temp, 0x0a
				    brsh q2_mode_recept
				    jmp quit2_mode_recept
				q2_mode_recept:
				    subi temp, 0x0a
				    inc r1
				    jmp cycle1_mode_recept
				quit2_mode_recept:
				    dec basebuf
				    mov adrLowX, basebuf
					ori temp, 0x30
				    st X,temp
				    dec basebuf
				    mov adrLowX, basebuf
					mov temp, r1
					ori temp, 0x30
				    st X,temp
					;send_string_from_data: ;�������������� �������� ����-�� ��� ������
						ldi temp, 0x03;���������� ��������� � ������
						mov ldiTemp, temp
					m1_1:	
						nop
						in adrLowY, PORTE	;tempKey:=portB
						ori adrLowY, (1<<2)
						out PORTE, adrLowY	;portA:=scan
						ldi adrLowY, 0
	 
						ld temp, X
						out PORTD, temp;
					 	;pulse
					 	in adrLowY, PORTE	;tempKey:=portB
						ori adrLowY, (1<<2)
						out PORTE, adrLowY	;portA:=scan
						ldi adrLowY, 0;

					 	ldi temp, 0x06
						delay_us_5:
							nop
							dec temp;
							cpi temp, 0;
							brne delay_us_5;

						in adrLowY, PORTE	;tempKey:=portB
						andi adrLowY, ~(1<<2)
						out PORTE, adrLowY	;portA:=scan
						ldi adrLowY, 0;

						ldi temp, 0x06
						delay_us_6:
							nop
							dec temp;
							cpi temp, 0;
							brne delay_us_6;

						inc XL
						BRCS increm_XH
						jmp afterinc_X
						increm_XH:	inc XH	
						afterinc_X:

						dec ldiTemp
						ldi temp, 0
						cp ldiTemp, temp
						brne m1_1
					;set_line_0:
					ldi temp,0x80			    
					;send_cmd
					in adrLowY, PORTE	;tempKey:=portB
					andi adrLowY, ~(1<<0)
					out PORTE, adrLowY	;portA:=scan
					ldi adrLowY, 0;

					out PORTD, temp;
					;pulse
				 	in adrLowY, PORTE	;tempKey:=portB
					ori adrLowY, (1<<2)
					out PORTE, adrLowY	;portA:=scan
					ldi adrLowY, 0;

				 	ldi temp, 0x06
					delay_us_21:
						nop
						dec temp;
						cpi temp, 0;
						brne delay_us_21;

					in adrLowY, PORTE	;tempKey:=portB
					andi adrLowY, ~(1<<2)
					out PORTE, adrLowY	;portA:=scan
					ldi adrLowY, 0;

					ldi temp, 0x06
					delay_us_22:
						nop
						dec temp;
						cpi temp, 0;
						brne delay_us_22
			jmp end_main_switch;
		mode_custom:
			mov temp, selectedCustomMenu;
			LSL	temp; x2
			ldi ZH, 0x00;
			mov ZL, temp

			subi ZL, low (-Table_update_display_custom_menu_switch)
			sbci ZH, high (-Table_update_display_custom_menu_switch)
			ijmp
			mode_heating:
					clr adrHighX;
					ldi adrLowX, 0x70;load from data
					ld temp, X;
				    ldi basebuf, 0x92
				cycle_heating:
				    cpi temp, 0x0a
				    brsh q1_heating
				    jmp quit1_heating
				q1_heating:
				    subi temp, 0x0a
				    inc r1     
				    jmp cycle_heating
				quit1_heating:
				    mov adrLowX, basebuf
					ori temp, 0x30
				    st X,temp
				    mov temp, r1
				    clr r1
				cycle1_heating:
				    cpi temp, 0x0a
				    brsh q2_heating
				    jmp quit2_heating
				q2_heating:
				    subi temp, 0x0a
				    inc r1
				    jmp cycle1_heating
				quit2_heating:
				    dec basebuf
				    mov adrLowX, basebuf
					ori temp, 0x30
				    st X,temp
				    dec basebuf
				    mov adrLowX, basebuf
					mov temp, r1
					ori temp, 0x30
				    st X,temp
					;send_string_from_data: ;�������������� �������� ����-�� ��� ������
						ldi temp, 0x03;���������� ��������� � ������
						mov ldiTemp, temp
					m1_2:	
						nop
						in adrLowY, PORTE	;tempKey:=portB
						ori adrLowY, (1<<2)
						out PORTE, adrLowY	;portA:=scan
						ldi adrLowY, 0
	 
						ld temp, X
						out PORTD, temp;
					 	;pulse
					 	in adrLowY, PORTE	;tempKey:=portB
						ori adrLowY, (1<<2)
						out PORTE, adrLowY	;portA:=scan
						ldi adrLowY, 0;

					 	ldi temp, 0x06
						delay_us_31:
							nop
							dec temp;
							cpi temp, 0;
							brne delay_us_31;

						in adrLowY, PORTE	;tempKey:=portB
						andi adrLowY, ~(1<<2)
						out PORTE, adrLowY	;portA:=scan
						ldi adrLowY, 0;

						ldi temp, 0x06
						delay_us_32:
							nop
							dec temp;
							cpi temp, 0;
							brne delay_us_32;

						inc XL
						BRCS increm_XH_1
						jmp afterinc_X_1
						increm_XH_1:	inc XH	
						afterinc_X_1:

						dec ldiTemp
						ldi temp, 0
						cp ldiTemp, temp
						brne m1_2
				jmp end_switch_mode_custom;
			mode_mixer:
					clr adrHighX;
					ldi adrLowX, 0x80;load from data
					ld temp, X;
				    ldi basebuf, 0x92
				cycle_mixer:
				    cpi temp, 0x0a
				    brsh q1_mixer
				    jmp quit1_mixer
				q1_mixer:
				    subi temp, 0x0a
				    inc r1     
				    jmp cycle_mixer
				quit1_mixer:
				    mov adrLowX, basebuf
					ori temp, 0x30
				    st X,temp
				    mov temp, r1
				    clr r1
				cycle1_mixer:
				    cpi temp, 0x0a
				    brsh q2_mixer
				    jmp quit2_mixer
				q2_mixer:
				    subi temp, 0x0a
				    inc r1
				    jmp cycle1_mixer
				quit2_mixer:
				    dec basebuf
				    mov adrLowX, basebuf
					ori temp, 0x30
				    st X,temp
				    dec basebuf
				    mov adrLowX, basebuf
					mov temp, r1
					ori temp, 0x30
				    st X,temp
					;send_string_from_data: ;�������������� �������� ����-�� ��� ������
						ldi temp, 0x03;���������� ��������� � ������
						mov ldiTemp, temp
					m1_3:	
						nop
						in adrLowY, PORTE	;tempKey:=portB
						ori adrLowY, (1<<2)
						out PORTE, adrLowY	;portA:=scan
						ldi adrLowY, 0
	 
						ld temp, X
						out PORTD, temp;
					 	;pulse
					 	in adrLowY, PORTE	;tempKey:=portB
						ori adrLowY, (1<<2)
						out PORTE, adrLowY	;portA:=scan
						ldi adrLowY, 0;

					 	ldi temp, 0x06
						delay_us_33:
							nop
							dec temp;
							cpi temp, 0;
							brne delay_us_33;

						in adrLowY, PORTE	;tempKey:=portB
						andi adrLowY, ~(1<<2)
						out PORTE, adrLowY	;portA:=scan
						ldi adrLowY, 0;

						ldi temp, 0x06
						delay_us_34:
							nop
							dec temp;
							cpi temp, 0;
							brne delay_us_34;

						inc XL
						BRCS increm_XH_2
						jmp afterinc_X_2
						increm_XH_2:	inc XH	
						afterinc_X_2:

						dec ldiTemp
						ldi temp, 0
						cp ldiTemp, temp
						brne m1_3
				jmp end_switch_mode_custom;
			mode_weigher:
				LDI ZL, LOW(menu_text21*2)
				LDI ZH, HIGH(menu_text21*2)
				;send_string: ;�������������� �������� ��� ������
				ldi temp, 0x10;���������� ��������� � ������
				mov ldiTemp, temp
			m4:	
				nop
				in adrLowX, PORTE	;tempKey:=portB
				ori adrLowX, (1<<2)
				out PORTE, adrLowX	;portA:=scan
				ldi adrLowX, 0
	 
				LPM 
				inc ZL
				BRCS increm_ZH_3
				jmp afterinc_3
				increm_ZH_3:	inc ZH	
				afterinc_3:
				out PORTD, r0;
			 	;pulse
			 	in adrLowY, PORTE	;tempKey:=portB
				ori adrLowY, (1<<2)
				out PORTE, adrLowY	;portA:=scan
				ldi adrLowY, 0;

			 	ldi temp, 0x06
				delay_us_29:
					nop
					dec temp;
					cpi temp, 0;
					brne delay_us_29;

				in adrLowY, PORTE	;tempKey:=portB
				andi adrLowY, ~(1<<2)
				out PORTE, adrLowY	;portA:=scan
				ldi adrLowY, 0;

				ldi temp, 0x06
				delay_us_30:
					nop
					dec temp;
					cpi temp, 0;
					brne delay_us_30;

				dec ldiTemp
				ldi temp, 0
				cp ldiTemp, temp
				brne m4
				jmp end_switch_mode_custom;
			end_switch_mode_custom:
				jmp end_main_switch
			jmp end_main_switch;
		mode_in_progress:
			LDI ZL, LOW(menu_intents*2)
			LDI ZH, HIGH(menu_intents*2)
			add ZL, currentPozitionOfMenu;
			add ZL, currentPozitionOfMenu;
			brcs increment_ZH_2;
			jmp after_increment_zh_2;
			increment_ZH_2: inc ZH
			after_increment_zh_2:
			LPM
			mov temp, r0
			LSL	temp; x2
			ldi ZH, 0x00;
			mov ZL, temp

			subi ZL, low (-Table_update_display_progress_intent)
			sbci ZH, high (-Table_update_display_progress_intent)
			ijmp
			intent_recept:
				clr adrHighX;
					ldi adrLowX, 0x20;load from data
					ldi adrHighX, 0x1; itogo 0x120
					ld temp, X;
					clr adrHighX
				    ldi basebuf, 0x92
				cycle_recept:
				    cpi temp, 0x0a
				    brsh q1_recept
				    jmp quit1_recept
				q1_recept:
				    subi temp, 0x0a
				    inc r1     
				    jmp cycle_recept
				quit1_recept:
				    mov adrLowX, basebuf
					ori temp, 0x30
				    st X,temp
				    mov temp, r1
				    clr r1
				cycle1_recept:
				    cpi temp, 0x0a
				    brsh q2_recept
				    jmp quit2_recept
				q2_recept:
				    subi temp, 0x0a
				    inc r1
				    jmp cycle1_recept
				quit2_recept:
				    dec basebuf
				    mov adrLowX, basebuf
					ori temp, 0x30
				    st X,temp
				    dec basebuf
				    mov adrLowX, basebuf
					mov temp, r1
					ori temp, 0x30
					st X,temp
					;send_string_from_data: ;�������������� �������� ����-�� ��� ������
						ldi temp, 0x03;���������� ��������� � ������
						mov ldiTemp, temp
					m1_4:	
						nop
						in adrLowY, PORTE	;tempKey:=portB
						ori adrLowY, (1<<2)
						out PORTE, adrLowY	;portA:=scan
						ldi adrLowY, 0
	 
						ld temp, X
						out PORTD, temp;
					 	;pulse
					 	in adrLowY, PORTE	;tempKey:=portB
						ori adrLowY, (1<<2)
						out PORTE, adrLowY	;portA:=scan
						ldi adrLowY, 0;

					 	ldi temp, 0x06
						delay_us_35:
							nop
							dec temp;
							cpi temp, 0;
							brne delay_us_35;

						in adrLowY, PORTE	;tempKey:=portB
						andi adrLowY, ~(1<<2)
						out PORTE, adrLowY	;portA:=scan
						ldi adrLowY, 0;

						ldi temp, 0x06
						delay_us_36:
							nop
							dec temp;
							cpi temp, 0;
							brne delay_us_36;

						inc XL
						BRCS increm_XH_3
						jmp afterinc_X_3
						increm_XH_3:	inc XH	
						afterinc_X_3:

						dec ldiTemp
						ldi temp, 0
						cp ldiTemp, temp
						brne m1_4
				jmp end_switch_progress
			intent_custom:
				mov temp, selectedCustomMenu
				LSL	temp; x2
				ldi ZH, 0x00;
				mov ZL, temp

				subi ZL, low (-Table_update_display_progress_custom)
				sbci ZH, high (-Table_update_display_progress_custom)
				ijmp
				progress_custom_heating:
						clr adrHighX;
						ldi adrLowX, 0x10;load from data
						ldi adrHighX, 0x1; itogo 0x110
						ld temp, X;
						clr adrHighX;
					    ldi basebuf, 0x92
					cycle_custom_heating:
					    cpi temp, 0x0a
					    brsh q1_custom_heating
					    jmp quit1_custom_heating
					q1_custom_heating:
					    subi temp, 0x0a
					    inc r1     
					    jmp cycle_custom_heating
					quit1_custom_heating:
					    mov adrLowX, basebuf
						ori temp, 0x30
					    st X,temp
					    mov temp, r1
					    clr r1
					cycle1_custom_heating:
					    cpi temp, 0x0a
					    brsh q2_custom_heating
					    jmp quit2_custom_heating
					q2_custom_heating:
					    subi temp, 0x0a
					    inc r1
					    jmp cycle1_custom_heating
					quit2_custom_heating:
					    dec basebuf
					    mov adrLowX, basebuf
						ori temp, 0x30
					    st X,temp
					    dec basebuf
					    mov adrLowX, basebuf
						mov temp, r1
						ori temp, 0x30
					    st X,temp
						;send_string_from_data: ;�������������� �������� ����-�� ��� ������
						ldi temp, 0x03;���������� ��������� � ������
						mov ldiTemp, temp
					m1_5:	
						nop
						in adrLowY, PORTE	;tempKey:=portB
						ori adrLowY, (1<<2)
						out PORTE, adrLowY	;portA:=scan
						ldi adrLowY, 0
	 
						ld temp, X
						out PORTD, temp;
					 	;pulse
					 	in adrLowY, PORTE	;tempKey:=portB
						ori adrLowY, (1<<2)
						out PORTE, adrLowY	;portA:=scan
						ldi adrLowY, 0;

					 	ldi temp, 0x06
						delay_us_37:
							nop
							dec temp;
							cpi temp, 0;
							brne delay_us_37;

						in adrLowY, PORTE	;tempKey:=portB
						andi adrLowY, ~(1<<2)
						out PORTE, adrLowY	;portA:=scan
						ldi adrLowY, 0;

						ldi temp, 0x06
						delay_us_38:
							nop
							dec temp;
							cpi temp, 0;
							brne delay_us_38;

						inc XL
						BRCS increm_XH_4
						jmp afterinc_X_4
						increm_XH_4:	inc XH	
						afterinc_X_4:

						dec ldiTemp
						ldi temp, 0
						cp ldiTemp, temp
						brne m1_5
					jmp end_switch_progress_custom
				progress_custom_mixer:
						clr adrHighX;
						ldi adrLowX, 0x20;load from data
						ldi adrHighX, 0x1; itogo 0x120
						ld temp, X;
						clr adrHighX;
					    ldi basebuf, 0x92
					cycle_custom_mixer:
					    cpi temp, 0x0a
					    brsh q1_custom_mixer
					    jmp quit1_custom_mixer
					q1_custom_mixer:
					    subi temp, 0x0a
					    inc r1     
					    jmp cycle_custom_mixer
					quit1_custom_mixer:
					    mov adrLowX, basebuf
						ori temp, 0x30
					    st X,temp
					    mov temp, r1
					    clr r1
					cycle1_custom_mixer:
					    cpi temp, 0x0a
					    brsh q2_custom_mixer
					    jmp quit2_custom_mixer
					q2_custom_mixer:
					    subi temp, 0x0a
					    inc r1
					    jmp cycle1_custom_mixer
					quit2_custom_mixer:
					    dec basebuf
					    mov adrLowX, basebuf
						ori temp, 0x30
					    st X,temp
					    dec basebuf
					    mov adrLowX, basebuf
						mov temp, r1
						ori temp, 0x30
					    st X,temp
						;send_string_from_data: ;�������������� �������� ����-�� ��� ������
						ldi temp, 0x03;���������� ��������� � ������
						mov ldiTemp, temp
					m1_6:	
						nop
						in adrLowY, PORTE	;tempKey:=portB
						ori adrLowY, (1<<2)
						out PORTE, adrLowY	;portA:=scan
						ldi adrLowY, 0
	 
						ld temp, X
						out PORTD, temp;
					 	;pulse
					 	in adrLowY, PORTE	;tempKey:=portB
						ori adrLowY, (1<<2)
						out PORTE, adrLowY	;portA:=scan
						ldi adrLowY, 0;

					 	ldi temp, 0x06
						delay_us_39:
							nop
							dec temp;
							cpi temp, 0;
							brne delay_us_39;

						in adrLowY, PORTE	;tempKey:=portB
						andi adrLowY, ~(1<<2)
						out PORTE, adrLowY	;portA:=scan
						ldi adrLowY, 0;

						ldi temp, 0x06
						delay_us_40:
							nop
							dec temp;
							cpi temp, 0;
							brne delay_us_40;

						inc XL
						BRCS increm_XH_5
						jmp afterinc_X_5
						increm_XH_5:	inc XH	
						afterinc_X_5:

						dec ldiTemp
						ldi temp, 0
						cp ldiTemp, temp
						brne m1_6
					jmp end_switch_progress_custom
				progress_custom_weigher:
						clr adrHighX;
						ldi adrLowX, 0x00;load from data
						ldi adrHighX, 0x1; itogo 0x100
						ld temp, X;
						clr adrHighX;
					    ldi basebuf, 0x92
					cycle_weigher:
					    cpi temp, 0x0a
					    brsh q1_weigher
					    jmp quit1_weigher
					q1_weigher:
					    subi temp, 0x0a
					    inc r1     
					    jmp cycle_weigher
					quit1_weigher:
					    mov adrLowX, basebuf
						ori temp, 0x30
					    st X,temp
					    mov temp, r1
					    clr r1
					cycle1_weigher:
					    cpi temp, 0x0a
					    brsh q2_weigher
					    jmp quit2_weigher
					q2_weigher:
					    subi temp, 0x0a
					    inc r1
					    jmp cycle1_weigher
					quit2_weigher:
					    dec basebuf
					    mov adrLowX, basebuf
						ori temp, 0x30
					    st X,temp
					    dec basebuf
					    mov adrLowX, basebuf
						mov temp, r1
						ori temp, 0x30
					    st X,temp
						;send_string_from_data: ;�������������� �������� ����-�� ��� ������
						ldi temp, 0x03;���������� ��������� � ������
						mov ldiTemp, temp
					m1_7:	
						nop
						in adrLowY, PORTE	;tempKey:=portB
						ori adrLowY, (1<<2)
						out PORTE, adrLowY	;portA:=scan
						ldi adrLowY, 0
	 
						ld temp, X
						out PORTD, temp;
					 	;pulse
					 	in adrLowY, PORTE	;tempKey:=portB
						ori adrLowY, (1<<2)
						out PORTE, adrLowY	;portA:=scan
						ldi adrLowY, 0;

					 	ldi temp, 0x06
						delay_us_41:
							nop
							dec temp;
							cpi temp, 0;
							brne delay_us_41;

						in adrLowY, PORTE	;tempKey:=portB
						andi adrLowY, ~(1<<2)
						out PORTE, adrLowY	;portA:=scan
						ldi adrLowY, 0;

						ldi temp, 0x06
						delay_us_42:
							nop
							dec temp;
							cpi temp, 0;
							brne delay_us_42;

						inc XL
						BRCS increm_XH_6
						jmp afterinc_X_6
						increm_XH_6:	inc XH	
						afterinc_X_6:

						dec ldiTemp
						ldi temp, 0
						cp ldiTemp, temp
						brne m1_7
					jmp end_switch_progress_custom
				end_switch_progress_custom:
				jmp end_switch_progress
			end_switch_progress:
				jmp end_main_switch;
			jmp end_main_switch;
	end_main_switch:
		nop;
jmp update_display;

Table_update_display_progress_custom:
jmp progress_custom_heating
jmp progress_custom_mixer
jmp progress_custom_weigher
end_Table_update_display_progress_custom: nop

Table_update_display_main_switch:
jmp mode_menu;
jmp mode_recept;
jmp mode_custom;
jmp mode_in_progress;
end_Table_update_display_main_switch: nop

Table_update_display_custom_menu_switch:
jmp mode_heating;
jmp mode_mixer;
jmp mode_weigher;
end_Table_update_display_custom_menu_switch: nop

Table_update_display_progress_intent:
jmp intent_recept;
jmp intent_custom;
end_Table_update_display_progress_intent: nop

menu_text:
menu_text1: .db 0x10,0x10,0x10,0x10,0x10,0x10,0xCF,0xEB,0xEE,0xE2,0x10,0x10,0x10,0x10,0x10,0x10; ����
menu_text2: .db 0x10,0xC3,0xF0,0xE5,0xF7,0xED,0xE5,0xE2,0xE0,0xFF,0x20,0xEA,0xE0,0xF8,0xE0,0x10; ��������� ����
menu_text3: .db 0x10,0x10,0x10,0xCC,0xE0,0xED,0xED,0xE0,0xFF,0x20,0xEA,0xE0,0xF8,0xE0,0x10,0x10; ������ ����
menu_text4: .db 0x10,0x10,0xCE,0xE2,0xF1,0xFF,0xED,0xE0,0xFF,0x20,0xEA,0xE0,0xF8,0xE0,0x10,0x10; ������� ����
menu_text5: .db 0x10,0x10,0x10,0xD0,0xE0,0xF1,0xF1,0xEE,0xEB,0xFC,0xED,0xE8,0xEA,0x10,0x10,0x10; ����������
menu_text6: .db 0x10,0x10,0x10,0x10,0x10,0x10,0xC1,0xEE,0xF0,0xF9,0x10,0x10,0x10,0x10,0x10,0x10; ����
menu_text7: .db 0x10,0x10,0x10,0x10,0x10,0x10,0x10,0xD9,0xE8,0x10,0x10,0x10,0x10,0x10,0x10,0x10; ��
menu_text8: .db 0x10,0x10,0x10,0x10,0x10,0x10,0xD5,0xE0,0xF0,0xF7,0xEE,0x10,0x10,0x10,0x10,0x10; �����
menu_text9: .db 0x10,0x10,0x10,0xC3,0xF0,0xE8,0xE1,0xED,0xEE,0xE9,0x20,0xF1,0xF3,0xEF,0x10,0x10; ������� ���
menu_text10: .db 0x10,0x10,0x10,0x10,0x10,0x10,0x10,0x10,0x10,0x10,0x10,0x10,0x10,0x10,0x10,0x10; �������
menu_text11: .db 0x10,0x10,0x10,0x10,0x10,0xCA,0xEE,0xF2,0xEB,0xE5,0xF2,0xFB,0x10,0x10,0x10,0x10; ��������
menu_text12: .db 0x10,0x10,0x10,0x10,0x10,0x10,0xCC,0xE0,0xED,0xF2,0xFB,0x10,0x10,0x10,0x10,0x10; �����
menu_text13: .db 0x10,0x10,0x10,0x10,0x10,0xD5,0xE8,0xED,0xEA,0xE0,0xEB,0xE8,0x10,0x10,0x10,0x10; �������
menu_text14: .db 0x10,0x10,0xCC,0xEE,0xEB,0xEE,0xF7,0xED,0xFB,0xE9,0x20,0xF1,0xF3,0xEF,0x10,0x10; �������� ���
menu_text15: .db 0x10,0x10,0x10,0xCA,0xF3,0xF0,0xE8,0xED,0xFB,0xE9,0x20,0xF1,0xF3,0xEF,0x10,0x10; ������� ���
menu_text16: .db 0x10,0x10,0x10,0x10,0x10,0x10,0x10,0xD3,0xF5,0xE0,0x10,0x10,0x10,0x10,0x10,0x10; ���
menu_text17: .db 0x10,0x10,0xCA,0xE0,0xF0,0xF2,0xEE,0xF4,0xE5,0xEB,0xFC,0x20,0xF4,0xF0,0xE8,0x10; ��������� ���
menu_text18: .db 0x10,0x10,0xC3,0xEE,0xF0,0xEE,0xF5,0xEE,0xE2,0xFB,0xE9,0x20,0xF1,0xF3,0xEF,0x10; ��������� ���
menu_text19: .db 0x10,0x10,0x10,0xCD,0xE0,0xE3,0xF0,0xE5,0xE2,0xE0,0xF2,0xE5,0xEB,0xFC,0x10,0x10; �����������
menu_text20: .db 0x10,0x10,0x10,0x10,0x10,0xCC,0xE8,0xEA,0xF1,0xE5,0xF0,0x10,0x10,0x10,0x10,0x10; ������
menu_text21: .db 0x10,0x10,0x10,0x10,0x10,0x10,0xC2,0xE5,0xF1,0xFB,0x10,0x10,0x10,0x10,0x10,0x10; ����
end_menu_text: nop;

menu_intents:
menu_intents1: .db 0x0;��������� - ������
menu_intents2: .db 0x0;��������� - ������
menu_intents3: .db 0x0;��������� - ������
menu_intents4: .db 0x0;��������� - ������
menu_intents5: .db 0x0;��������� - ������
menu_intents6: .db 0x0;��������� - ������
menu_intents7: .db 0x0;��������� - ������
menu_intents8: .db 0x0;��������� - ������
menu_intents9: .db 0x0;��������� - ������
menu_intents10: .db 0x0;��������� - ������
menu_intents11: .db 0x0;��������� - ������
menu_intents12: .db 0x0;��������� - ������
menu_intents13: .db 0x0;��������� - ������
menu_intents14: .db 0x0;��������� - ������
menu_intents15: .db 0x0;��������� - ������
menu_intents16: .db 0x0;��������� - ������
menu_intents17: .db 0x0;��������� - ������
menu_intents18: .db 0x0;��������� - ������
menu_intents19: .db 0x1;��������� - ��������� �����
menu_intents20: .db 0x2;��������� - ��������� �����
menu_intents21: .db 0x3;��������� - ��������� �����
end_menu_intents: nop;


