.include "m103def.inc"

.def posH=r16
.def temp=r17
.def posV=r18
.def tempKey=r19
.def key=r20
.def flags=r21
.def flags2=r22
.def scan=r23
.def basebuf=r24
.def flags3=r25

.def adrLowX=r26; low X
.def adrHighX=r27; High X
.def adrLowY=r28; low Y
.def adrHighY=r29; High Y
.def adrLowZ=r30; low Z
.def adrHighZ=r31; High Z

.def bufposH=r2
.def bufscan=r3

.def mode=r4;mode ����� ��������� �������� 
			;menu = 0
			;recept = 1
			;custom = 2
			;inProgress = 3

.def currentPozitionOfMenu=r5; ������� ����� ����
.def maxPointMenu=r6; ����������� ����������� �������� currentPozitionOfMenu

.def selectedPointMenu=r7; ��������� ����� ����
.def selectedCustomMenu=r8; ��������� ������ �����
					;0 - ������
					;1 - ������
					;2 - ����
.def ldiTemp=r9; temp ��� ������ ��������


.org 0x00
jmp start;
.org 0x18
jmp int_count_compare
.org 0x1c
jmp int_real_timer
.org 0x30
start:
	;����� ��������� ��������x ��� ������ ��������
	ldi basebuf, 0x60
	ldi posH, 0x0
	ldi scan, 0xEE
	ldi flags, 0x0
	ldi flags2, 0x0
	ldi flags3, 0x0

	;����� ��������� �� ��������x ��� ������ ��������
	ldi temp, 0x0;
	mov bufposH, temp;	
	mov bufscan, temp;	
	ldi temp, 0x00;
	mov mode, temp;	
	
	;��������� ��������� ��������x ���������
	ldi temp, 0x0
	out EIMSK, temp
	ldi temp, 0x14
	out TIMSK, temp
	ldi temp, 0xa4
	out OCR1AH, temp
	ldi temp, 0x73
	out OCR1AL, temp
	ldi temp, 0xf0
	out DDRB, temp	
	ldi temp, 0x0
	out DDRD, temp
	ldi temp, 0xff
	out DDRA, temp ; �����
	ldi temp, 0x05
	out DDRE, temp ; �����
	ldi temp, LOW(RAMEND)
	out spl, temp
	ldi temp, HIGH(RAMEND)
	out sph, temp
	SEI
	ldi adrLowX, 0x45
	ldi adrHighX, 0x01
	ldi temp, 0x00
	st X, temp
	clr adrLowX
	clr adrHighX	
	// ������ ������� ��������� �������
	sBR flags3, 0x40 ;f23
	ldi temp,  0x00     ; stop timer
	out tccr1b,  temp      ; set contolling frequency to the (no input)
	ldi temp,  0xa4     ; for 6 MHz
	out tcnt1h,  temp      ; load timerH
	ldi temp,  0x72     ; for 6 MHz
	out tcnt1l,  temp      ; load timerL
	in temp, tccr1b
	ori temp,  0x04      ; start timer
	out tccr1b,  temp    ; set contolling frequency to the exact match f(OSC)/1024 (with prescaler)
	jmp fon

fon:
	SBRC flags3, 5  ;f22
		jmp proc_int_real_timer
m21: 	
	jmp fon

.org 0x070f
int_real_timer:
	ldi temp,  0xa4     ; for 6 MHz
	out tcnt1h,  temp      ; load timerH
	ldi temp,  0x72     ; for 6 MHz
	out tcnt1l,  temp      ; load timerL
	in temp, tccr1b
	SBR flags3, 0x20 ;f22
	reti

int_count_compare:
	SBR flags3, 0x20 ;f22
	ldi temp, 0xa4
	out OCR1AH, temp
	ldi temp, 0x74
	out OCR1AL, temp
	reti

proc_int_real_timer:
	CBR flags3, 0x20 ;f22

	SBRC flags3, 6  ;f23
		jmp calc_recept_time
	m22:
	ldi adrLowX, 0x55 //load sec
	ldi adrHighX, 0x01
	ld temp, X;
	cpi temp, 0x3B //59 sec
	breq adr155_full
	jmp adr155_not_full
	adr155_full:
		ldi temp, 0x00 //load sec 00
		st X, temp
		ldi adrLowX, 0x54
		ldi adrHighX, 0x01
		ld temp, X;
		inc temp // inc min
		st X, temp;
		jmp after_analize_sec
	adr155_not_full:
		inc temp // inc sec
		st X, temp;
		jmp end_proc_int_real_timer
	after_analize_sec:
		ldi adrLowX, 0x54 // load ed min
		ldi adrHighX, 0x01
		ld temp, X;
		cpi temp, 0x0A
		breq adr154_full
			jmp adr154_not_full
		adr154_full:
			ldi temp, 0x00
			st X, temp
			ldi adrLowX, 0x53 // load des min
			ldi adrHighX, 0x01
			ld temp, X;
			inc temp //inc des min
			st X, temp;
			jmp after_analize_ed_min;
		adr154_not_full:
			jmp end_proc_int_real_timer;
	after_analize_ed_min:
			ldi adrLowX, 0x53 // load des min
			ldi adrHighX, 0x01
			ld temp, X;
			cpi temp, 0x06
			breq adr153_full
				jmp adr153_not_full
		adr153_full:
			ldi temp, 0x00
			st X, temp
			ldi adrLowX, 0x52 // load hours
			ldi adrHighX, 0x01
			ld temp, X;
			inc temp //inc hours
			st X, temp;
			jmp after_analize_des_min;
		adr153_not_full:
			jmp end_proc_int_real_timer;
	after_analize_des_min:							
			ldi adrLowX, 0x52 // load hours
			ldi adrHighX, 0x01
			ld temp, X;
			cpi temp, 0x19 //25 hours
			breq adr152_full
				jmp adr152_not_full
		adr152_full:
			ldi temp, 0x00
			st X, temp
			jmp after_analize_hours;
		adr152_not_full:	
			jmp after_analize_hours;
	after_analize_hours:
			ldi adrLowX, 0x52 // load hours
			ldi adrHighX, 0x01
			ld temp, X;
			cpi temp, 0x15 //19 hours
			brge adr152_ge_20
				jmp adr152_lower_20
		adr152_ge_20:
			ldi adrLowX, 0x51 // load hours
			ldi adrHighX, 0x01
			ld temp, X;
			cpi temp, 0x03 //20 hours
			breq adr151_4
				jmp adr151_not_4
			adr151_4:
				ldi temp, 0x00
				st X, temp
				ldi adrLowX, 0x50 // load hours
				ldi adrHighX, 0x01
				st X, temp
				ldi adrLowX, 0x52 // load hours
				ldi adrHighX, 0x01
				st x, temp
				jmp end_proc_int_real_timer
			adr151_not_4:
				inc temp
				st X, temp
				jmp end_proc_int_real_timer
		adr152_lower_20:			
			ldi adrLowX, 0x51 // load hours
			ldi adrHighX, 0x01
			ld temp, X;
			cpi temp, 0x09 //20 hours
			brge adr151_9
				jmp adr151_not_9
			adr151_9:
				ldi temp, 0x00
				st X, temp
				ldi adrLowX, 0x50 // load hours
				ldi adrHighX, 0x01
				ld temp, X
				inc temp
				st X, temp
				jmp after_analize_ed_hours
			adr151_not_9:
				inc temp
				st X, temp
				jmp end_proc_int_real_timer	
	after_analize_ed_hours:		
end_proc_int_real_timer:
	jmp m21


calc_recept_time:
	ldi adrLowX, 0x40
	ldi adrHighX, 0x01
	ld temp, X;
	cpi temp, 0x00
	breq adr140_null
	jmp adr140_not_null		
adr140_null:	
	ldi adrLowX, 0x41
	ldi adrHighX, 0x01
	ld temp, X
	cpi temp, 0x00
	breq adr141_null
	jmp adr141_not_null
	adr141_null:
		SBR flags3, 0x08 ;f19
		CBR flags3, 0x40 ;f23
		jmp m22
	adr141_not_null:
		dec temp
		st X, temp
		jmp m22
adr140_not_null:
	ldi adrLowX, 0x41
	ldi adrHighX, 0x01
	ld temp, X
	cpi temp, 0x00
	breq adr141_null_1
	jmp adr141_not_null_1
	adr141_null_1:
		ldi temp, 0x3b
		st X, temp
		ldi adrLowX, 0x40
		ldi adrHighX, 0x01
		ld temp, X
		dec temp
		st X, temp
		jmp m22
	adr141_not_null_1:
		dec temp
		st X, temp
		jmp m22


































