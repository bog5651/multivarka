.include "m103def.inc"

.def posH=r16
.def temp=r17
.def posV=r18
.def tempKey=r19
.def key=r20
.def flags=r21
.def flags2=r22
.def scan=r23
.def basebuf=r24
.def flags3=r25

.def adrLowX=r26; low X
.def adrHighX=r27; High X
.def adrLowY=r28; low Y
.def adrHighY=r29; High Y
.def adrLowZ=r30; low Z
.def adrHighZ=r31; High Z

.def bufposH=r2
.def bufscan=r3

.def mode=r4;mode ����� ��������� �������� 
			;menu = 0
			;recept = 1
			;custom = 2
			;inProgress = 3

.def currentPozitionOfMenu=r5; ������� ����� ����
.def maxPointMenu=r6; ����������� ����������� �������� currentPozitionOfMenu

.def selectedPointMenu=r7; ��������� ����� ����
.def selectedCustomMenu=r8; ��������� ������ �����
					;0 - ������
					;1 - ������
					;2 - ����
.def ldiTemp=r9; temp ��� ������ ��������

.org 0x00
jmp start;
.org 0x1c
jmp int_real_timer
.org 0x2a
jmp int_adc
.org 0x30
start:
	;����� ��������� ��������x ��� ������ ��������
	ldi basebuf, 0x60
	ldi posH, 0x0
	ldi scan, 0xEE
	ldi flags, 0x0
	ldi flags2, 0x0
	ldi flags3, 0x0

	;����� ��������� �� ��������x ��� ������ ��������
	ldi temp, 0x0;
	mov bufposH, temp;	
	mov bufscan, temp;	
	ldi temp, 0x00;
	mov mode, temp;	
	
	;��������� ��������� ��������x ���������
	ldi temp, 0x0
	out EIMSK, temp
	ldi temp, 0x14
	out TIMSK, temp
	ldi temp, 0xa8
	out OCR1AH, temp
	ldi temp, 0x9d
	out OCR1AL, temp
	ldi temp, 0xf0
	out DDRB, temp	
	ldi temp, 0x0
	out DDRD, temp
	ldi temp, 0xff
	out DDRA, temp ; �����
	ldi temp, 0x0D
	out DDRE, temp ; �����
	ldi temp, LOW(RAMEND)
	out spl, temp
	ldi temp, HIGH(RAMEND)
	out sph, temp
	SEI
	ldi adrLowX, 0x45
	ldi adrHighX, 0x01
	ldi temp, 0x00
	st X, temp
	ldi adrLowX, 0x60
	ldi adrHighX, 0x01
	ldi temp, 0x0e
	st X, temp
	ldi adrLowX, 0x83
	ldi adrHighX, 0x00
	ldi temp, 0x00
	st X, temp
	ldi adrLowX, 0x76
	ldi adrHighX, 0x00
	ldi temp, 0xc8
	st X, temp
	clr adrLowX
	clr adrHighX	
	// ������ ������� ��������� �������
	sBR flags3, 0x40 ;f23
	ldi temp,  0x00     ; stop timer
	out tccr1b,  temp      ; set contolling frequency to the (no input)
	ldi temp,  0xa4     ; for 6 MHz
	out tcnt1h,  temp      ; load timerH
	ldi temp,  0x72     ; for 6 MHz
	out tcnt1l,  temp      ; load timerL
	in temp, tccr1b
	ori temp,  0x04      ; start timer
	out tccr1b,  temp    ; set contolling frequency to the exact match f(OSC)/1024 (with prescaler)
	jmp fon

fon:
	SBRC flags3, 5  ;f22
		jmp proc_int_real_timer
m20:
	ldi adrLowX, 0x61
	ldi adrHighX, 0x01
	ld temp, X
	SBRC temp, 0  ;161.0
		jmp proc_int_adc
m_adc:
	ldi adrLowX, 0x60
	ldi adrHighX, 0x01
	ld temp, X
	SBRC temp, 1  ;160.1
		jmp proc_do_heat
m_heat:
	ldi adrLowX, 0x60
	ldi adrHighX, 0x01
	ld temp, X
	SBRC temp, 2  ;160.2
		jmp proc_do_cool
m_cool:
	ldi adrLowX, 0x60
	ldi adrHighX, 0x01
	ld temp, X
	SBRC temp, 3  ;160.3
		jmp proc_start_adc
m_str_adc:
	jmp fon

.org 0x070f
int_real_timer:
	ldi temp,  0xa4     ; for 6 MHz
	out tcnt1h,  temp      ; load timerH
	ldi temp,  0x72     ; for 6 MHz
	out tcnt1l,  temp     ; load timerL	
	ldi temp, 0xa8
	out OCR1AH, temp
	ldi temp, 0x9b
	out OCR1AL, temp
	in temp, tccr1b
	SBR flags3, 0x20 ;f22
	reti

int_adc:
	ldi adrLowX, 0x61
	ldi adrHighX, 0x01
	ld temp, X
	SBR temp, 0x01 ;161.0
	st X, temp
	reti;

proc_int_adc://161.0
	//0x0072 ������� ���-��
	ldi adrLowX, 0x61
	ldi adrHighX, 0x01
	ld temp, X
	CBR temp, 0x01
	st X, temp;
	in temp, ADCL
	in ldiTemp, ADCH
	lsl ldiTemp;
	lsl ldiTemp;
	lsl ldiTemp;
	lsl ldiTemp;
	lsl ldiTemp;
	lsl ldiTemp;

	lsr temp;
	lsr temp;

	or temp, ldiTemp;

	ldi adrLowX, 0x72
	ldi adrHighX, 0x00
	st X, temp
	jmp m_adc;

proc_do_heat://160.1
	ldi adrLowX, 0x60
	ldi adrHighX, 0x01
	ld temp, X
	CBR temp, 0x02
	st X, temp;
	in temp, PINE
	SBR temp, 0x08
	out PORTE, temp;
	jmp m_heat;

proc_do_cool://160.2
	ldi adrLowX, 0x60
	ldi adrHighX, 0x01
	ld temp, X
	CBR temp, 0x04
	st X, temp;
	in temp, PINE
	CBR temp, 0x08
	out PORTE, temp;
	jmp m_cool;

proc_start_adc://160.3
	ldi adrLowX, 0x60
	ldi adrHighX, 0x01
	ld temp, X
	CBR temp, 0x08
	st X, temp;

	ldi temp, 0b00000000
	out ADMUX, temp
	ldi temp, 0b11011000
	out ADCSR, temp; //1(adc en)1(start)0(cycle)0(nop)1(int)000(prescaler)

	jmp m_str_adc;

proc_int_real_timer:
	CBR flags3, 0x20
	ldi adrLowX, 0x61
	ldi adrHighX, 0x01
	ld temp, X
	SBRC temp, 1  ;161.1
	jmp dec_heater_timer
m1_1:
	jmp m20

dec_heater_timer:
	ldi adrLowX, 0x71
	ldi adrHighX, 0x00
	ld temp, X
	dec temp
	cpi temp, 0x00
	breq stop_heater
	st X, temp

	ldi adrLowX, 0x72
	ldi adrHighX, 0x00
	ld temp, X
	ldi adrLowX, 0x08
	add temp, adrLowX//temp = current

	ldi adrLowX, 0x70
	ldi adrHighX, 0x00
	ld ldiTemp, X //ldiTemp = need
	
	//if(current+10>need) need=0x0070
	cp temp, ldiTemp 
	BRSH if_true_1
	jmp if_false_1
	if_true_1:
		//do_cool
		ldi adrLowX, 0x60
		ldi adrHighX, 0x01
		ld temp, X
		SBR temp, 0x04 // 160.2
		st X, temp
		jmp end_if_1
	if_false_1:
		//do_heat
		ldi adrLowX, 0x60
		ldi adrHighX, 0x01
		ld temp, X
		SBR temp, 0x02 // 160.1
		st X, temp
	end_if_1:
	ldi adrLowX, 0x72
	ldi adrHighX, 0x00
	ld temp, X
	ldi adrLowX, 0x08
	sub ldiTemp, adrLowX
	//if(current<need-10)
	cp temp, ldiTemp 
	BRLO if_true_2
	jmp if_false_2
	if_true_2:
		//do_heat
		ldi adrLowX, 0x60
		ldi adrHighX, 0x01
		ld temp, X
		SBR temp, 0x02 // 160.1
		st X, temp
		jmp end_if_2
	if_false_2:
		//do_cool
		ldi adrLowX, 0x60
		ldi adrHighX, 0x01
		ld temp, X
		SBR temp, 0x04 // 160.2
		st X, temp
	end_if_2:
	ldi adrLowX, 0x60
	ldi adrHighX, 0x01
	ld temp, X
	SBR temp, 0x08 // 160.3
	jmp m1_1
stop_heater:
	ldi adrLowX, 0x61
	ldi adrHighX, 0x01
	ld temp, X
	CBR temp, 0x02 //161.1
	st X, temp
	ldi adrLowX, 0x60
	ldi adrHighX, 0x01
	ld temp, X
	SBR temp, 0x04 // 160.2
	st X, temp
	jmp m1_1

